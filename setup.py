"""Inference of CRN through greedy heuristics."""

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="statCRI",
    version="0.0.1",
    author="Julien Martinelli",
    author_email="julien.martinelli@inria.fr",
    description="CRN inference through greedy heuristics",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.inria.fr/julmarti/statCRI",
    license="MIT",
    install_requires=["numpy", "scipy", "findiff", "pysindy"],
    tests_requires=["pytest"],
    python_requires=">=3",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
