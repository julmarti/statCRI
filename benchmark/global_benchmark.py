"""Global benchmark against parameter change."""

import functools
import multiprocessing as mp
import numpy as np
import pickle as pkl
from argparse import ArgumentParser

from statCRI.evaluate import random_crn, run_model_global


def parallel_run_model_global(networks, kinetics,
                              catalysts, npts, T, exp,
                              delta, tau, alpha, seed, modif, x):
    saved_args = locals()
    saved_args[modif] = x
    # if time horizon is parallelized, npts needs it as well
    if modif == 'T':
        saved_args['npts'] = x * 10 + 1
    # 10 arguments in run_model_global
    mod_dict = {k: saved_args[k] for k in list(saved_args)[:10]}
    return run_model_global(*mod_dict.values())


def main(save):
    seed = 983
    np.random.seed(seed)
    n_crns = 25
    networks, kinetics, catalysts = [], [], []

    # generate the CRNs and initial conditions
    for _ in range(n_crns):
        crn, K, cata = random_crn()
        networks.append(crn)
        kinetics.append(K)
        catalysts.append(cata)

    inputs = {}
    inputs['CRNs'] = networks
    inputs['kinetics'] = kinetics
    inputs['catalysts'] = catalysts

    ### Base values for unchanged variables ###

    tau, alpha, delta = .9, .5, np.linspace(1, 3, 21)
    T, npts = 10, 101
    nexp = 3
    F1 = {}

    pool = mp.Pool()

    ### Evaluate against available number of traces ###

    print('evaluating nexp')
    F1_nexp = {}
    F1_nexp['grid'] = np.array(np.append(np.linspace(1, 10, 10),
                                         np.linspace(12, 20, 5)),
                               dtype=np.int32)

    P_tau = pool.map(functools.partial(parallel_run_model_global, networks,
                                       kinetics, catalysts, npts, T,
                                       None, delta, tau, 0,
                                       seed, 'exp'), F1_nexp['grid'])
    P_alpha = pool.map(functools.partial(parallel_run_model_global, networks,
                                         kinetics, catalysts, npts, T,
                                         None, delta, 0, alpha,
                                         seed, 'exp'), F1_nexp['grid'])

    F1_nexp['scores_energy'] = np.array(P_tau).T
    F1_nexp['scores_cv'] = np.array(P_alpha).T
    F1['nexp'] = F1_nexp

    ### Evaluate against time horizon change ###

    print('evaluating time horizon')
    F1_T = {}
    F1_T['grid'] = np.array(np.append(np.linspace(1, 10, 10),
                                      np.linspace(15, 40, 6)),
                            dtype=np.int32)

    P_tau = pool.map(functools.partial(parallel_run_model_global, networks,
                                       kinetics, catalysts, None, None,
                                       nexp, delta, tau, 0,
                                       seed, 'T'), F1_T['grid'])

    P_alpha = pool.map(functools.partial(parallel_run_model_global, networks,
                                         kinetics, catalysts, None, None,
                                         nexp, delta, 0, alpha,
                                         seed, 'T'), F1_T['grid'])

    F1_T['scores_energy'] = np.array(P_tau).T
    F1_T['scores_cv'] = np.array(P_alpha).T
    F1['T'] = F1_T

    ### Evaluate against tau change ###

    print('evaluating tau')
    F1_tau = {}
    F1_tau['grid'] = np.linspace(0.05, 1, 20)

    P_tau = pool.map(functools.partial(parallel_run_model_global, networks,
                                       kinetics, catalysts, npts, T,
                                       nexp, delta, None, 0,
                                       seed, 'tau'), F1_tau['grid'])

    F1_tau['scores'] = np.array(P_tau).T
    F1['tau'] = F1_tau

    ### Evaluate against alpha change ###

    print('evaluating alpha')
    F1_alpha = {}
    F1_alpha['grid'] = np.linspace(0.05, 2, 20)

    P_alpha = pool.map(functools.partial(parallel_run_model_global, networks,
                                         kinetics, catalysts, npts, T,
                                         nexp, delta, 0, None,
                                         seed, 'alpha'), F1_alpha['grid'])

    F1_alpha['scores'] = np.array(P_alpha).T
    F1['alpha'] = F1_alpha

    results = {}
    results['inputs'] = inputs
    results['F1'] = F1

    with open(f'global_bench_results_compare_{save}.dat', 'wb') as f:
        pkl.dump(results, f)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-s",
                        "--save",
                        default=None,
                        type=str,
                        help="Save the results on a dict.")
    main(**vars(parser.parse_args()))
