#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
for i in `seq 1 5`;
do
    echo running crn $i
    python3 $SCRIPT_DIR/benchmark.py -n 5 -t .9 -a .6 -s $SCRIPT_DIR/results/crn$i
done
