"""Benchmarking against SINDY."""

import numpy as np
import pysindy as ps

from scipy.integrate import odeint
from statCRI.transitions import Transitions
from statCRI.algorithm import StatCRI
from statCRI.evaluate import f1_score, get_sde, precision_recall, random_crn, random_y0, sde
from statCRI.utils import print_reaction


seed = np.random.randint(0, 10000)
np.random.seed(seed)
print(f'seed is {seed}\n')
T, nexp, npts = 10, 5, 101
t = np.linspace(0, T, npts)
safe = 0

while not safe:
    crn, K, cata = random_crn()
    nspecies = crn.shape[1]
    exps = np.zeros([nexp, npts, nspecies])
    for i in range(nexp):
        y0 = random_y0(nspecies)
        exps[i] = odeint(sde, y0, t, args=(get_sde(crn, K, cata),))
    if not np.isnan(exps).any() and exps.max() < 1e10 and exps.min() >= 0:
        safe = 1

print('initial conditions:')

for n in range(nexp):
    print(exps[n, 0, :])
print('\n')
name_species = [f's{i}' for i in range(nspecies)]

transitions = Transitions(exps)
algo = StatCRI(exps, transitions.dxdt, name_species,
               **{"tau": .9, "delta": np.linspace(1, 3, 20)})
algo.fit()

print('Learned CRN')
algo.print_crn()
print('\nTrue CRN')
for reac, k, c in zip(crn, K, cata):
    print_reaction(name_species, reac, k, c)

precision, recall = precision_recall(algo.crn, crn, cata)
f1 = f1_score(precision, recall)
print(f'\nPrecision:{precision}\nRecall:{recall}\nF1-score:{f1}')

### SINDY

library_functions = [
    lambda x, y, z: x * y * z,
    lambda x, y: x**2 * y,
    lambda x, y: x * y**2
]
library_function_names = [
    lambda x, y, z: x + ' ' + y + ' ' + z,
    lambda x, y: x + '^2 ' + y,
    lambda x, y: x + ' ' + y + '^2'
]
custom_library = ps.CustomLibrary(library_functions=library_functions,
                                  function_names=library_function_names)

print('\nSINDY')
poly_library = ps.PolynomialLibrary(degree=2)
feature_library = poly_library + custom_library
optimizer = ps.SR3(max_iter=100000)
model = ps.SINDy(feature_library=feature_library,
                 optimizer=optimizer,
                 feature_names=name_species)
model.fit([algo.x[i] for i in range(nexp)],
          x_dot=[algo.dxdt[i] for i in range(nexp)],
          t=t, multiple_trajectories=True)
model.print()
