"""Benchmark on randomly generated CRN with random initial conditions."""

import numpy as np
import pickle as pkl

from scipy.integrate import odeint
from statCRI.transitions import Transitions
from statCRI.algorithm_cv import StatCRI as StatCRIcv
from statCRI.algorithm_energy import StatCRI as StatCRIenergy
from statCRI.evaluate import check_minset, compute_simulations, f1_score, get_sde, precision_recall, sde
from statCRI.utils import parse_arguments, print_crn, str_reaction
from toy_crns import chain, mapk, parallel, product_parallel, reactant_parallel, loop, tyson, tyson_ode # noqa


# def main(crn_name, nexp, tau, alpha, save=False, seed=None):
def main(crn_name, nexp, tau, alpha, verbose, save=False, seed=None):
    # seed = np.random.randint(0, 10000) if seed is None else seed
    # np.random.seed(seed)
    # print(f'seed is {seed}\n')
    T, npts = 10, 101
    t = np.linspace(0, T, npts)

    func_crn_name = globals()[crn_name]
    crn, K, cata, name_species, y0 = func_crn_name()
    nspecies = len(name_species)

    # tyson ODEs cannot be obtained from the reactions with my implementation
    if nexp == 1:
        exps = np.zeros([1, npts, nspecies])
        if crn_name == 'tyson':
            exps[0] = odeint(tyson_ode, y0, t, args=(K,))
        else:
            exps[0] = odeint(sde, y0, t, args=(get_sde(crn, K, cata),))
    else:
        if crn_name == 'tyson':
            exps = compute_simulations(crn, K, cata, nexp, t, tyson_ode, K, seed=seed)
        else:
            exps = check_minset(crn_name, crn, K, cata, nexp, t)
            # exps = compute_simulations(crn, K, cata, nexp, t, seed=seed)

    print('initial conditions:')
    for n in range(nexp):
        print(exps[n, 0, :])
    print('\n')

    transitions = Transitions(exps)
    delta = np.linspace(1, 3, 21)

    algocv = StatCRIcv(exps, transitions.dxdt,
                       name_species,
                       **{"alpha": alpha,
                          "delta": delta,
                          "verbose": verbose})

    algoenergy = StatCRIenergy(exps, transitions.dxdt,
                               name_species,
                               **{"tau": tau,
                                  "delta": delta,
                                  "verbose": verbose})

    algocv.fit()
    algoenergy.fit()
    print('Learned CRN: coefficient of variation minimization\n')
    print_crn(algocv.crn, name_species)
    print('\nLearned CRN: energy minimization\n')
    print_crn(algoenergy.crn, name_species)

    print('\nTrue CRN')
    for reac, k, c in zip(crn, K, cata):
        print(str_reaction(name_species, reac, k, c))

    precision_cv, recall_cv = precision_recall(algocv.crn, crn, cata)
    precision_energy, recall_energy = precision_recall(algoenergy.crn, crn, cata)
    f1_cv = f1_score(precision_cv, recall_cv)
    f1_energy = f1_score(precision_energy, recall_energy)
    print(f'\nCV - Precision:{precision_cv}\nRecall:{recall_cv}\nF1-score:{f1_cv}')
    print(f'\nEnergy - Precision:{precision_energy}\nRecall:{recall_energy}\nF1-score:{f1_energy}')

    if save:
        methods = ['energy', 'cv']
        keys = ['crn', 'precision', 'recall', 'f1']
        precisions = [precision_energy, precision_cv]
        recalls = [recall_energy, recall_cv]
        f1s = [f1_energy, f1_cv]
        crns = [algoenergy.crn, algocv.crn]
        values = [crns, precisions, recalls, f1s]
        savedict = {}
        for i, m in enumerate(methods):
            savedict[m] = {}
            for k, v in zip(keys, values):
                savedict[m][k] = v[i]
        savedict['ground_truth'] = (crn, K, cata)
        with open(f'{save}_{crn_name}.dat', 'wb') as f:
            pkl.dump(savedict, f)


if __name__ == '__main__':
    parser = parse_arguments()
    main(**vars(parser.parse_args()))
