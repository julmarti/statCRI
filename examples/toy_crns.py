"""Implementation of example CRNs."""

import numpy as np


def chain():

    """
    Chain CRN
    MA(1) for A=>B
    MA(1) for B=>C
    MA(1) for C=>D
    MA(1) for D=>E
    """

    nreacs, nspecies = 4, 5
    params = np.ones(nreacs)
    name_species = [chr(ord('A') + i) for i in range(nspecies)]
    y0 = np.array([1, 0, 0, 0, 0])
    crn = np.array([[-1, 1, 0, 0, 0],
                    [0, -1, 1, 0, 0],
                    [0, 0, -1, 1, 0],
                    [0, 0, 0, -1, 1]])
    cata = -1 * np.ones(nreacs)
    return crn, params, cata, name_species, y0


def parallel():

    """
    Parallel CRN
    MA(3) for A+D=>B+D
    MA(2) for C=>D
    MA(1) for E=>F
    """

    params = np.array([2, 1, 3])
    nspecies = 6
    name_species = [chr(ord('A') + i) for i in range(nspecies)]
    y0 = np.array([1, 0, 1, 0, 1, 0])
    crn = np.array([[0, 0, -1, 1, 0, 0],
                    [0, 0, 0, 0, -1, 1],
                    [-1, 1, 0, 0, 0, 0]])
    cata = np.array([-1, -1, 3])
    return crn, params, cata, name_species, y0


def reactant_parallel():

    """
    Reactant Parallel CRN
    MA(3) for A+D=>B+D
    MA(2) for C=>D
    MA(1) for E=>D
    """

    params = np.array([2, 1, 3])
    nspecies = 5
    name_species = [chr(ord('A') + i) for i in range(nspecies)]
    y0 = np.array([1, 0, 1, 0, 1])
    crn = np.array([[0, 0, -1, 1, 0],
                    [0, 0, 0, 1, -1],
                    [-1, 1, 0, 0, 0]])
    cata = np.array([-1, -1, 3])
    return crn, params, cata, name_species, y0


def product_parallel():

    """
    Product Parallel CRN
    MA(1) for A+C=>B+C
    MA(7) for C=>D
    MA(4) for C=>E
    """

    params = np.array([1, 7, 4])
    nspecies = 5
    name_species = [chr(ord('A') + i) for i in range(nspecies)]
    y0 = np.array([1, 0, 1, 0, 0])
    crn = np.array([[0, 0, -1, 1, 0],
                    [0, 0, -1, 0, 1],
                    [-1, 1, 0, 0, 0]])
    cata = np.array([-1, -1, 2])
    return crn, params, cata, name_species, y0


def loop():

    """
    Loop CRN
    MA(1) for A=>B
    MA(1) for B=>C
    MA(1) for C=>D
    MA(1) for D=>E
    MA(1) for E=>A
    """

    nreacs, nspecies = 5, 5
    params = np.ones(nreacs)
    name_species = [chr(ord('A') + i) for i in range(nspecies)]
    y0 = np.array([1, 0, 0, 0, 0])
    crn = np.array([[-1, 1, 0, 0, 0],
                    [0, -1, 1, 0, 0],
                    [0, 0, -1, 1, 0],
                    [0, 0, 0, -1, 1],
                    [1, 0, 0, 0, -1]])
    cata = -1 * np.ones(nreacs)
    return crn, params, cata, name_species, y0


def mapk():

    """
    MAPK cascade simplified to its first two levels
    adapted from Qiao et al. Bisability and Oscillations in the Huang-Ferrell
    model of MAPK signaling, PLOS comp. biol. 3(9) 2007

    MA(150) for KKK =[E1]=> KKKp.
    MA(150) for KKKp =[E2]=> KKK.
    MA(1000) for KKKp+KK => KKKpKK.
    MA(150) for KKKpKK => KKKp+KK.
    MA(150) for KKKpKK => KKKp+KKp.
    MA(150) for KKp =[E3]=> KK.
    MA(1000) for KKKp+KKp => KKKpKKp.
    MA(150) for KKKpKKp => KKKp+KKp.
    MA(150) for KKKpKKp => KKKp+KKpp.
    MA(150) for KKpp =[E3]=> KKp.
    """

    params = np.array([150, 150, 1000, 150, 150, 150, 1000, 150, 150, 150])
    name_species = ['KK', 'KKp', 'KKpp', 'KKK', 'KKKp', 'KKKpKK', 'KKKpKKp', 'E1', 'E2', 'E3']
    y0 = np.array([1.2, 0, 0, 3e-3, 0, 0, 0, 3e-5, 3e-4, 3e-4])
    cata = np.array([7, 8, -1, -1, -1, 9, -1, -1, -1, 9])
    crn = np.array([[0, 0, 0, -1, 1, 0, 0, 0, 0, 0],
                    [0, 0, 0, 1, -1, 0, 0, 0, 0, 0],
                    [-1, 0, 0, 0, -1, 1, 0, 0, 0, 0],
                    [1, 0, 0, 0, 1, -1, 0, 0, 0, 0],
                    [0, 1, 0, 0, 1, -1, 0, 0, 0, 0],
                    [1, -1, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, -1, 0, 0, -1, 0, 1, 0, 0, 0],
                    [0, 1, 0, 0, 1, 0, -1, 0, 0, 0],
                    [0, 0, 1, 0, 1, 0, -1, 0, 0, 0],
                    [0, 1, -1, 0, 0, 0, 0, 0, 0, 0]])
    return crn, params, cata, name_species, y0


def tyson():

    """
    Model of the cell cycled based on the interactions between Cdc2 and Cyclin.
    Tyson (1991) - Modeling the cell division cycle: cdc2 and cyclin interactions.
    PNAS, 88: 7328-7332

    MA(0.015)   for _=>Cyclin
    MA(200)   for Cyclin+Cdc2P => Cdc2CyclinPP.
    MA(0.018)  for Cdc2CyclinPP => Cdc2CyclinP.
    MA(180)   for Cdc2CyclinPP + 2* Cdc2CyclinP => 3*Cdc2CyclinP.
    MA(1)   for Cdc2CyclinP => CyclinP+Cdc2.
    MA(0.6)   for CyclinP =>_.
    MA(10000)   for Cdc2 => Cdc2P.
    MA(100)   for Cdc2P => Cdc2.
    """

    params = np.array([.015, 200, .018, 180, 1, .6, 10000, 100])
    name_species = ['Cdc2', 'Cdc2P', 'CyclinP', 'Cdc2CyclinP', 'Cdc2CyclinPP', 'Cyclin']
    y0 = np.array([1, 0, 0, 0, 0, 0])
    cata = np.ones(8)
    crn = np.array([[0, 0, 0, 0, 0, 1],
                    [0, -1, 0, 0, 1, -1],
                    [0, 0, 0, 1, -1, 0],
                    [0, 0, 0, 1, -1, 0],
                    [1, 0, 1, -1, 0, 0],
                    [0, 0, -1, 0, 0, 0],
                    [-1, 1, 0, 0, 0, 0],
                    [1, -1, 0, 0, 0, 0]])
    cata = - np.ones(len(crn), dtype=np.int32)
    cata[3] = 4
    return crn, params, cata, name_species, y0


def tyson_ode(y, t, K):
    return [K[0] * y[3] + K[2] * y[1] - K[1] * y[0],
            K[1] * y[0] - (K[3] * y[5] + K[2]) * y[1],
            K[0] * y[3] - K[4] * y[2],
            (K[5] + K[6] * y[3]**2) * y[4] - K[0] * y[3],
            K[3] * y[1] * y[5] - (K[5] + K[6] * y[3]**2) * y[4],
            K[7] - K[3] * y[1] * y[5]]
