"""This file contains functions shared by the different implementations."""

import numpy as np
import matplotlib.pyplot as plt

from numba import jit
from argparse import ArgumentParser
from matplotlib.lines import Line2D


@jit(nopython=True)
def get_reactions_from_group(group):

    """
    From a group of n_species, get all possible
    reactions involving n_species.

    Args:
    group: numpy boolean vector, active species.

    Returns:
    reaction_matrix: numpy matrix of -1, 0 and 1s.

    Example:
    get_reactions_from_group((1, 0, 0, 1))
    ==> array([[-1,  0,  0,  1],
               [ 1,  0,  0, -1],
               [ 1,  0,  0,  1],
               [ -1, 0,  0, -1])
    """

    npgrp = np.array(group)
    idx = np.where(npgrp == 1)[0]
    if len(idx) == 1:
        return np.vstack((npgrp, - npgrp))
    if len(idx) == 2:
        z1 = np.copy(npgrp)
        npgrp[idx[0]] = -1
        return np.vstack((z1, -z1, npgrp, - npgrp))
    else:
        z = [np.copy(npgrp), np.copy(npgrp), npgrp]
        for i in range(3):
            z[i][idx[i]] = -1
        return np.vstack((z[0], z[1], z[2], -z[0], -z[1], -z[2]))


def get_skeletons(dxdt, nspecies, delta):

    """
    Get groups of species whose variations match.

    Returns:
    groups: a dict whose keys are boolean vectors representing
    species belonging to a similar groups. The value associated
    to a key is a numpy array of timepoints used to infer the group.
    """

    abs_dxdt = np.abs(dxdt)
    groups, skeleton_matrix = {}, np.zeros([0, nspecies])
    argmax_species = np.argmax(abs_dxdt, axis=2)
    # cannot avoid looping over different traces so far.
    for d in delta:
        for exp in range(abs_dxdt.shape[0]):
            for i in range(abs_dxdt.shape[1]):
                group = tuple((abs_dxdt[exp, i, argmax_species[exp, i]] <= ((1 + d) * abs_dxdt[exp, i])).astype(int))
                nterms = len(np.nonzero(group)[0])
                current = [exp, i]
                if group not in groups.keys() and nterms < 4:
                    groups[group] = np.array(current)
                    skeleton_matrix = np.r_[skeleton_matrix,
                                            get_reactions_from_group(group)]
                elif group in groups.keys() and not [exp, i] in groups[group].tolist():
                    groups[group] = np.vstack((groups[group],
                                               np.array(current)))
    return skeleton_matrix, groups


def print_crn(crn, species_names):

    """Print learned CRN."""

    for n in range(len(crn)):
        reaction = crn[f'reaction {n}']
        print(str_reaction(species_names, reaction['skeleton'],
                       reaction['kinetics'], reaction['cata']))


def str_reaction(species, *reaction):

    """
    Pretty print of a learned reaction.

    Args:
    species: list of molecular species.
    reaction: either the dict entries 'vector', 'kinetics' and 'cata' if
    reaction was learnt by the algorithm, or the vector, kinetics and cata
    if the reaction is generated.

    Returns:
    str with easy to read inferred reaction.

    Example:
    print_reaction(algo.species_names,
                   algo.crn['reaction 0']['skeleton'],
                   algo.crn['reaction 0']['kinetics'],
                   algo.crn['reaction 0']['cata'])
    """

    reactants = np.where(reaction[0] == -1)[0]
    products = np.where(reaction[0] == 1)[0]
    k, cata = np.round(reaction[1], 5), reaction[2]

    left_term = ''
    right_term = ''

    if not reactants.size:
        left_term += '_'
    elif not products.size:
        right_term += '_'
    if reactants.size:
        if reactants.size == 1:
            left_term = species[reactants[0]]
        else:
            left_term = species[reactants[0]] + ' + ' + species[reactants[1]]
    if products.size:
        if products.size == 1:
            right_term = species[products[0]]
        else:
            right_term = species[products[0]] + ' + ' + species[products[1]]
    kinetics = f'MA({k})'
    cata = '' if cata == -1 else f' with catalyst {species[cata]}'
    return kinetics + ' for ' + left_term + ' => ' + right_term + cata


def plot_support(algo, t):

    """Plot the timepoints used for the inference of each reaction.

    Args:
    algo: StatCRI fitted object.
    t: numpy vector, timegrid proper to the simulations.

    Returns:
    A series of plots with as many rows as inferred reactions and
    as many columns as number of experiments.
    """

    n_updates, nexp = len(algo.store_updates), len(algo.x)
    assert(n_updates > 0)
    nspecies = len(algo.species_names)
    position = 1
    fig, ax = plt.subplots(n_updates, nexp,
                           figsize=(5 * nexp, 5 * n_updates),
                           sharex='col', sharey='col')
    for row in range(n_updates):
        for col in range(nexp):
            axes = fig.add_subplot(n_updates, nexp, position)
            axes.plot(t, algo.store_updates[row][col],
                      marker='o', markersize=2, linewidth=.5)
            support = algo.crn[f'reaction {row}']['supp']
            support = support[support[:, 0] == col][:, 1]
            for j in range(nspecies):
                if algo.crn[f'reaction {row}']['skeleton'][j]:
                    axes.plot(t[support],
                              algo.store_updates[row][col, support, j],
                              color='black', marker='o',
                              markersize=3, linestyle='None')
            position += 1
    # trick to still enjoy the share options while adding subplots
    [[a.set_xticks([]) for a in aa] for aa in ax]
    [[a.set_yticks([]) for a in aa] for aa in ax]

    lines = [Line2D([0], [0], color=f'C{i}',
             marker='o', markersize=5) for i in range(nspecies)]
    fig.legend(lines, algo.species_names, ncol=nspecies,
               bbox_to_anchor=(.75, .95), fontsize=18)


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("-n",
                        "--nexp",
                        help="int, number of experiments",
                        type=int,
                        default=None)
    parser.add_argument("-t",
                        "--tau",
                        help="float, relative decrease",
                        type=float,
                        default=None)
    parser.add_argument("-a",
                        "--alpha",
                        help="float, coefficient of variation threshold",
                        type=float,
                        default=None)
    parser.add_argument("-c",
                        "--crn_name",
                        default=None,
                        help="str, crn name",
                        type=str)
    parser.add_argument("-se",
                        "--seed",
                        default=None,
                        help="int, random seed",
                        type=int)
    parser.add_argument("-s",
                        "--save",
                        default=None,
                        type=str,
                        help="Save the results on a dict.")
    parser.add_argument("-v",
                        "--verbose",
                        default=None,
                        type=int,
                        help="Boolean, verbose for algorithm")
    return parser
