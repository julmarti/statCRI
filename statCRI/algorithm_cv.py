import numpy as np

from numba import jit
from .transitions import Transitions
from .utils import get_skeletons


class StatCRI(Transitions):

    def __init__(self, x, dxdt, species_names=None, **params):
        super(StatCRI, self).__init__(x, dxdt)
        for attr_name, attr_value in params.items():
            setattr(self, attr_name, attr_value)
        self.species_names = species_names
        self.nspecies = len(self.species_names)
        self.list_rm_skel = []
        self.crn = {}
        self.cv_thisround = self.alpha
        self.store_updates = []

    @property
    def dxdt(self):
        return self._dxdt

    @dxdt.setter
    def dxdt(self, dxdt):
        self._dxdt = dxdt

    def get_mal(self, skeleton, cata=-1):

        """
        Compute mass action law coefficient for a reaction.

        Args:
        skeleton: numpy array of size nspecies. entries are -1
        for a reactant and 1 for a product.

        Returns:
        reaction: dict including score of the reaction, mass action law
        parameter, reactants and products, vector form of the reaction.
        """

        r = np.where(skeleton == -1)[0]
        p = np.where(skeleton == 1)[0]
        denom = self.x[:, :, r].prod(axis=2, keepdims=True)
        if cata != -1:
            denom *= self.x[:, :, cata, np.newaxis]  # ugly
        species = np.append(r, p)
        cv, k = np.inf, 0
        for s in species:
            sgn = -1 if s in r else 1
            numerator = sgn * self.dxdt[:, :, s][:, :, np.newaxis]
            k_dist_s = np.divide(numerator, denom,
                                 out=1e100 * np.ones_like(numerator),
                                 where=denom > 1e-8)
            k_dist_s = k_dist_s[k_dist_s != 1e100]
            # triggers if numerator and denominator have disjoint supports
            k_mean_s, cv_species = get_mean_cv(k_dist_s)
            if cv_species < cv and k_mean_s > 0:
                cv = cv_species
                k = k_mean_s
        if self.verbose:
            print(f'{skeleton} / k {k} / cv {cv} / cata {cata}')
        return {'cv': cv,
                'kinetics': k,
                'skeleton': skeleton,
                'cata': cata,
                'mal_product': denom}

    def selector(self, reaction):

        """
        Later
        """

        skeleton_w_cata = np.append(reaction['skeleton'], reaction['cata']).tolist()
        if reaction['cv'] < self.cv_thisround and skeleton_w_cata not in self.list_rm_skel:
            self.best_reaction = reaction
            self.cv_thisround = self.best_reaction['cv']

    def transition_update(self, reac):

        """
        Update the difference vector with the removal of the effect
        of the inferred reaction.

        Args:
        reac: dict outputted by get_mal.
        """

        self.dxdt -= reac['mal_product'] * reac['skeleton'] * reac['kinetics']

    def fit(self):

        """
        Main CRN inference loop.

        Returns:
        crn: a dict containing each learned reaction.
        """

        while self.cv_thisround <= self.alpha:
            skeleton_matrix, explainers = get_skeletons(self.dxdt,
                                                        self.nspecies,
                                                        self.delta)
            self.best_reaction, self.cv_thisround = {'skeleton': np.nan}, np.inf
            for skeleton in skeleton_matrix:
                reaction = self.get_mal(skeleton)
                self.selector(reaction)

            # try catalyst if no reaction w/ low enough cv
            if self.cv_thisround > self.alpha:
                for skeleton in skeleton_matrix:
                    if (skeleton == -1).sum() < 2:
                        for c in range(self.nspecies):
                            reaction = self.get_mal(skeleton, c)
                            self.selector(reaction)

            if self.cv_thisround < self.alpha:
                self.best_reaction['supp'] = explainers[tuple(self.best_reaction['skeleton']**2)]
                self.store_updates.append(np.copy(self.dxdt))
                self.crn[f'reaction {len(self.crn)}'] = self.best_reaction
                self.transition_update(self.best_reaction)
                if self.verbose:
                    print(f'\ncv of best reaction is {self.cv_thisround}\n')

                # list needed to ensure selected reaction not inferred again next time.
                skeleton_w_cata = np.append(self.best_reaction['skeleton'],
                                            self.best_reaction['cata'])
                self.list_rm_skel.append(skeleton_w_cata.tolist())

        del self.cv_thisround
        del self.best_reaction
        del self.list_rm_skel
        return self.crn


@jit(nopython=True)
def get_mean_cv(dist):

    """
    Compute mean estimate and unbiased estimate for
    coefficient of variation.

    Args:
    dist: numpy vector of samples

    Returns:
    mean: scalar, mean.
    cv: scalar, coefficient of variation.
    """

    # handle the case where mean is 0 or absence of samples
    if not dist.size:
        mean = 0.
        nsamples = 1
    else:
        mean = dist.mean()
        nsamples = len(dist)

    cv = dist.std() / np.abs(mean) if mean else np.inf
    cv *= (1 + 1 / (4 * nsamples))
    return mean, cv

