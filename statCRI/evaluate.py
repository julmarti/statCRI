"""Functions needed to create a benchmarking and evaluation protocol."""

import itertools
import numpy as np

from scipy.integrate import odeint
from .algorithm_energy import StatCRI as StatCRIenergy
from .algorithm_cv import StatCRI as StatCRIcv
from .transitions import Transitions


def random_reaction(nspecies, k_prior='log_uniform', catalyst_mean=.2):

    """
    Generate a random reaction.

    Args:
    nspecies: int, number of species in the network.
    k_prior: str, distribution from which kinetics rate are sampled.
    catalyst_mean: float, probability for a reaction to be catalyzed.


    Returns:
    vector: vector of -1, 0 and 1s describing reactants and products.
    k: float, mass action law coefficient of the reaction.
    cata: int, index of catalyst species. -1 if none.
    """

    # get kinetic rate
    if k_prior == 'log_uniform':
        k = 10**(np.random.uniform(-2, 2))
    elif k_prior == 'uniform':
        k = np.random.uniform(0, 100)
    elif k_prior == 'exp':
        k = np.random.exponential(100)
    elif k_prior == 'gaussian':
        k = np.random.normal(100, 50)

    # get number of species involved in reaction
    max_nterms = int(np.minimum(nspecies, 3))
    # handle the case where there is only 2 species in crn.
    p = [.5, .5] if max_nterms == 2 else [.25, .5, .25]
    nterms = np.random.choice([i for i in range(1, max_nterms + 1)], p=p)

    # select nterms species to constitute the reaction
    species = np.random.choice(nspecies, nterms, replace=False)
    vector = np.zeros(nspecies)
    vector[species] += np.random.choice([-1, 1], nterms)
    n_reactants = sum(vector == -1)

    # get catalyst, but not for reaction with already two reactants.
    if np.random.choice([0, 1], p=[1 - catalyst_mean, catalyst_mean]) and n_reactants < 2:
        cata = np.random.choice(nspecies)
    else:
        cata = -1
    return np.append(vector, cata), k


def random_crn(nspecies_prior_mean=4):

    """
    Generate a random crn.

    Args:
    nspecies_prior_mean: int, mean of the truncated Poisson distribution
    from which the number of species in the crn is sampled.
    nreac_prior_mean: int, mean of the truncated Poisson distribution
    from which the number of reactions in the crn is sampled.

    Returns:
    crn: numpy matrix size (nreac, nspecies).
    K: numpy vector of size nreac.
    cata: numpy vector of size nreac.
    """

    nspecies = truncated_Poisson(nspecies_prior_mean, min_value=2)
    max_val = 6 if nspecies == 2 else np.inf
    nreac = truncated_Poisson(nspecies, min_value=1,
                              max_value=max_val)
    crn, n = np.zeros([nreac, nspecies + 1]), 0
    K, cata = np.zeros(nreac), np.zeros(nreac, dtype=np.int32)
    while n < nreac:
        vector, k = random_reaction(nspecies)
        # avoid: reaction that are only double/triple degradation.
        # and triple synthesis
        # as well as to generate an already existing reaction.
        if np.sum(vector[:-1]) > -2 and np.sum(vector[:-1]) < 3 and not any(np.equal(crn, vector).all(1)):
            crn[n] = vector
            K[n] = k
            cata[n] = vector[-1]
            n += 1
    return crn[:, :-1], K, cata


def get_sde(crn, K, cata):

    """
    Map a CRN to a system of differential equations.

    Args:
    crn: numpy matrix size (nreac, nspecies) outputted by random_crn().
    K: numpy vector of size nreac outputted by random_crn().
    cata: numpy vector of size nreac outputted by random_crn().

    Returns:
    dsdt: dict
    """

    nreacs, nspecies = crn.shape
    dsdt = {}
    for ns in range(nspecies):
        dsdt[f'ds_{ns}/dt'] = []
    for nr in range(nreacs):
        R = np.where(crn[nr] == -1)[0]
        P = np.where(crn[nr] == 1)[0]
        Rc = np.r_[R, int(cata[nr])] if cata[nr] != -1 else R
        for r in R:
            dsdt[f'ds_{r}/dt'].append((Rc, - K[nr]))
        for p in P:
            dsdt[f'ds_{p}/dt'].append((Rc, K[nr]))
    return dsdt


def sde(y, t, eqs):

    """Callable for scipy.integrate.odeint."""

    list_ode = []
    for eq in eqs.values():
        terms = 0
        for e in eq:
            terms += np.prod(y[e[0]]) * e[1]
        list_ode.append(terms)
    return list_ode


def compute_simulations(crn, K, cata, nexp, t,
                        seed=None,
                        ode=sde, args=None,
                        presence_proba=.5):

    """
    From a crn, compute simulations.
    """

    # np.random.seed(seed)
    if args is None:
        args = get_sde(crn, K, cata)
    nspecies, npts = crn.shape[1], len(t)
    exps = np.zeros([nexp, npts, nspecies])
    # remove empty y0
    init_conds = list(itertools.product([0, 1], repeat=nspecies))[1:]
    i = 0
    while i < nexp:
        if init_conds:
            idx = np.random.choice(range(len(init_conds)))
            bool_y0 = init_conds[idx]
        # when all init cond have been sampled, switch back
        # to sampling y0s with presence proba .5 per species.
        else:
            bool_y0 = np.random.choice([0, 1], nspecies,
                                       p=[1 - presence_proba, presence_proba])
        concentration = np.random.uniform(0, 10, len(bool_y0))
        # hardcoded fix for MAPK crn
        if list(cata) == [7, 8, -1, -1, -1, 9, -1, -1, -1, 9]:
            concentration[7:] = np.random.uniform(1e-5, 1e-3, 3)
        y0 = bool_y0 * concentration
        simu = odeint(ode, y0, t, args=(args,))
        if not np.isnan(simu).any() and exps.max() < 1e10:
            exps[i] = simu
            i += 1
            if init_conds:
                init_conds.pop(idx)
    return exps


def check_minset(crn_name, *args):

    """Very bruteforce check for standard CRNs."""

    cond = False
    while not cond:
        exps = compute_simulations(*args)
        y0 = np.copy(exps[:, 0, :])
        y0[y0 > 0] = 1
        if crn_name == 'chain':
            y0 = np.sum(y0, axis=0)
            cond = True if y0[0] else False
        elif crn_name == 'product_parallel':
            cond_A_C = (y0[:, [0, 2]].sum(axis=1) == 2).any()
            cond = True if y0[:, 2].any() and cond_A_C else False
        elif crn_name == 'reactant_parallel' or 'parallel':
            ite = [2, 3, 4] if crn_name == 'reactant_parallel' else [2, 3]
            for i in ite:
                cond = cond or (y0[:, [0, i]].sum(axis=1) == 2).any()
            cond = True if cond and y0[:, 2].any() and y0[:, 4].any() else False
        elif crn_name == 'loop':
            cond = True
    return exps


def precision_recall(learned_crn, gt_crn, gt_cata):

    """
    Compute precision and recall based on ground truth
    and inferred reactions.

    Args:
    learned_crn: dict, crn learned by the algorithm.
    gt_crn: numpy matrix of size (n_true_reacs, nspecies).
    gt_cata: numpy vector of size n_true_reacs.

    Returns:
    precision: float in [0, 1].
    recall: float in [0, 1].
    """

    gt_list = gt_crn.tolist()
    TP, FP, FN = 0, 0, 0
    if len(learned_crn):
        for reac in learned_crn.values():
            # kinetics updated to 0
            if reac['kinetics']:
                if reac['skeleton'].tolist() in gt_list:
                    index = gt_list.index(reac['skeleton'].tolist())
                    if reac['cata'] == gt_cata[index]:
                        TP += 1
                    else:
                        FP += 1
                else:
                    FP += 1
                FN = len(gt_crn) - TP
                precision, recall = TP / (TP + FP), TP / (TP + FN)
    else:
        precision, recall = 0, 0
    return precision, recall


def f1_score(precision, recall):

    """
    Compute F1-score.

    Args:
    precision: float in [0, 1]
    recall: float in [0, 1]

    Returns:
    f1_score: float in [0, 1]
    """

    return 2 * (precision * recall) / (precision + recall) if precision + recall else 0


def run_model_global(networks, kinetics, catalysts,
                     npts, T, nexp, delta, tau, alpha, seed):

    """
    Run of the algorithm for benchmarking.
    """

    print(T, nexp, tau, alpha)
    t = np.linspace(0, T, npts)
    n_crns = len(networks)
    F1 = np.zeros(n_crns)
    for n, crn, K, cata in zip(range(n_crns), networks, kinetics, catalysts):
        nspecies = crn.shape[1]
        exps = compute_simulations(crn, K, cata, nexp, t, seed=seed)
        name_species = [f's{i}' for i in range(nspecies)]
        transitions = Transitions(exps)
        if tau:
            algo = StatCRIenergy(exps, transitions.dxdt, name_species,
                                 **{"tau": tau, "delta": delta})
        else:
            algo = StatCRIcv(exps, transitions.dxdt, name_species,
                             **{"alpha": alpha, "delta": delta})
        algo.fit()
        precision, recall = precision_recall(algo.crn, crn, cata)
        F1[n] = f1_score(precision, recall)
    return F1


def truncated_Poisson(mu, min_value, max_value=np.inf, size=1):

    """Sample from a truncated (from below) Poisson law.

    Args:
    mu: float, parameter of the Poisson Law.
    min_value: int, minimal value under which samples are rejected

    Return: int, truncated Poisson law sample."""

    sample = np.random.poisson(mu)
    return sample if sample >= min_value and sample <= max_value else truncated_Poisson(mu, min_value)
