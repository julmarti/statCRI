import numpy as np

from numba import jit
from scipy.optimize import minimize
from .transitions import Transitions
from .utils import get_skeletons


class StatCRI(Transitions):

    def __init__(self, x, dxdt, species_names=None, **params):
        super(StatCRI, self).__init__(x, dxdt)
        for attr_name, attr_value in params.items():
            setattr(self, attr_name, attr_value)
        self.species_names = species_names
        self.nspecies = len(self.species_names)
        self.list_rm_skel = []
        self.crn = {}
        self.energy_thisround, self.energy = 0, np.sum(self.dxdt**2)
        self.e_0 = np.copy(self.energy)
        self.store_updates = []

    @property
    def dxdt(self):
        return self._dxdt

    @dxdt.setter
    def dxdt(self, dxdt):
        self._dxdt = dxdt

    def get_mal(self, skeleton, cata=-1, k_old=0):

        """
        Compute mass action law coefficient for a reaction.

        Args:
        reaction_matrix: numpy (nreac, nspecies) matrix. Entries are
        -1 for a reactant and 1 for a product.

        Returns:
        reaction: dict including score of the reaction, mass action law
        parameter, reactants and products, vector form of the reaction.
        """

        k, energy = 0, self.energy
        r = np.where(skeleton == -1)[0]
        denom = self.x[:, :, r].prod(axis=2, keepdims=True)
        if cata != -1:
            denom *= self.x[:, :, cata, np.newaxis]
        if denom.any():
            res = minimize(comp_energy, 1,
                           args=(self.dxdt, denom, skeleton),
                           method='L-BFGS-B', bounds=[(k_old, None)])
            k, energy = res.x[0], res.fun
        if self.verbose:
            print(f'{skeleton} / k {k} / energy {energy/self.e_0 * 100} / cata {cata}')
        return {'energy': energy,
                'kinetics': k,
                'skeleton': skeleton,
                'cata': cata,
                'mal_product': denom}

    def selector(self, reaction):

        """
        Later
        """

        skeleton_w_cata = np.append(reaction['skeleton'], reaction['cata']).tolist()
        if reaction['energy'] < self.energy_thisround and skeleton_w_cata not in self.list_rm_skel:
            self.best_reaction = reaction
            self.energy_thisround = self.best_reaction['energy']

    def transition_update(self, reac):

        """
        Update the difference vector with the removal of the effect
        of the inferred reaction.

        Args:
        reac: dict outputted by get_mal.
        """

        self.dxdt -= reac['mal_product'] * reac['skeleton'] * reac['kinetics']

    def fit(self):

        """
        Main CRN inference loop.

        Returns:
        crn: a dict containing each learned reaction.
        """

        while self.energy_thisround < self.tau * self.energy:
            skeleton_matrix, explainers = get_skeletons(self.dxdt,
                                                        self.nspecies,
                                                        self.delta)
            self.best_reaction, self.energy_thisround = {'skeleton': np.nan}, np.inf
            for skeleton in skeleton_matrix:
                reaction = self.get_mal(skeleton)
                self.selector(reaction)

            # try catalyst if no reaction w/ low enough energy
            if self.energy_thisround > self.tau * self.energy:
                for skeleton in skeleton_matrix:
                    if (skeleton == -1).sum() < 2:
                        for c in range(self.nspecies):
                            reaction = self.get_mal(skeleton, c)
                            self.selector(reaction)

            if self.energy_thisround < self.tau * self.energy:
                self.best_reaction['supp'] = explainers[tuple(self.best_reaction['skeleton']**2)]
                self.crn[f'reaction {len(self.crn)}'] = self.best_reaction
                self.energy = self.energy_thisround
                self.store_updates.append(self.dxdt)
                self.transition_update(self.best_reaction)
                if self.verbose:
                    print(f'\nenergy of the system after update is {self.energy_thisround / self.e_0 * 100}\n')

                # list needed to ensure selected reaction not inferred again next time.
                skeleton_w_cata = np.append(self.best_reaction['skeleton'], self.best_reaction['cata'])
                self.list_rm_skel.append(skeleton_w_cata.tolist())

            # ugly but don't know any other way to exit
            else:
                break
            self.energy_thisround = 0
        del self.energy_thisround
        del self.best_reaction
        del self.list_rm_skel
        return self.crn


@jit(nopython=True)
def comp_energy(k, dxdt, denom, skeleton):

    """
    Considering a system dX/dt = f(X) and a reaction effect described
    by g(X), compute ||f(X) - g(X)||^2 the energy of the
    system under effect of a novel reaction. g is the product of
    species concentrations times a mass action law coefficient.

    Args:
    k: mass action law kinetics of inferred reaction.
    denom: vector of product of species concentration.
    vector: vector of -1, 0, 1s.

    Returns:
    energy: scalar.
    """

    return np.sum((dxdt - denom * k * skeleton)**2)
