import numpy as np

from findiff import FinDiff


class Transitions:

    """Class for data preprocessing into our internal representation."""

    def __init__(self, x, dxdt=None):
        check_data(x)
        self.x = x
        self.dxdt = dxdt

    # needed for the StatCRI class to modify the transitions
    @property
    def dxdt(self, dt=.1):
        d_dt = FinDiff(1, dt)
        self._dxdt = d_dt(self.x)
        return self._dxdt

    @dxdt.setter
    def dxdt(self, dxdt):
        self._dxdt = dxdt

    def moving_average(self):

        """Moving average smoothing to deal with stochastic simulation data."""

        pass

    def check_transitions(self):

        """Check for discrepancies in the transitions."""

        if (self.dxdt == 0).all():
            raise ValueError("System at steady state.")

    def subsampling(self):

        pass


def check_data(x):

    """Check for discrepancies in the data."""

    if np.isnan(x).any():
        raise ValueError("NaN species concentrations.")
    # integrator can go below 0
    # if (x < 0).any():
    #    raise ValueError("Negative species concentrations.")
